import request from '@/utils/request'

// 查询销售出库单列表
export function listProductsalse(query) {
  return request({
    url: '/mes/wm/productsalse/list',
    method: 'get',
    params: query
  })
}

// 查询销售出库单详细
export function getProductsalse(salseId) {
  return request({
    url: '/mes/wm/productsalse/' + salseId,
    method: 'get'
  })
}

// 新增销售出库单
export function addProductsalse(data) {
  return request({
    url: '/mes/wm/productsalse',
    method: 'post',
    data: data
  })
}

// 修改销售出库单
export function updateProductsalse(data) {
  return request({
    url: '/mes/wm/productsalse',
    method: 'put',
    data: data
  })
}

// 删除销售出库单
export function delProductsalse(salseId) {
  return request({
    url: '/mes/wm/productsalse/' + salseId,
    method: 'delete'
  })
}

//执行出库
export function execute(salseId) {
  return request({
    url: '/mes/wm/productsalse/' + salseId,
    method: 'put'
  })
}

// 根据客户 id 查询销售产品信息
export function getItem(clientId) {
  return request({
    url: '/mes/wm/productsalse/getItem/' + clientId,
    method: 'get'
  })
}

// 根据客户 id 查询销售订单信息
export function getSaleRecord(clientId) {
  return request({
    url: '/mes/wm/productsalse/getSaleRecord/' + clientId,
    method: 'get'
  })
}
